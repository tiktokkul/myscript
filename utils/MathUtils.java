package utils;

public class MathUtils {

    public static int roundToNearest10(int num) {
        return (int)(Math.round(num/10.0) * 10);
    }

    public static int clamp(int num, int min, int max) {
        return Math.max(Math.min(num,max),min);
    }
}
