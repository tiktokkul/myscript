package utils;

import javax.swing.*;
import java.util.function.Consumer;

public class UI {
    /**
     * Hollow factory/decorator for JLabels
     * @param actions
     * @return
     */
    @SafeVarargs
    public static JPanel createJPanel(Consumer<JPanel>... actions) {
        JPanel panel = new JPanel();
        for(Consumer<JPanel> action: actions) {
            action.accept(panel);
        }
        return panel;
    }

    /**
     * Hollow factory/decorator for JLabels
     * @param actions
     * @return
     */
    @SafeVarargs
    public static JLabel createJLabel(Consumer<JLabel>... actions) {
        JLabel label = new JLabel();
        for(Consumer<JLabel> action: actions) {
            action.accept(label);
        }
        return label;
    }
}
