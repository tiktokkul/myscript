package utils.uicomponents;

import javax.swing.*;

public interface Panelled {
    JPanel getPanel();
}
