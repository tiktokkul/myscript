package utils.uicomponents;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.util.function.IntConsumer;
import java.util.function.Supplier;

public class IntSpinner implements Panelled {
    private JSpinner spinner;
    private JLabel label;
    private SpinnerNumberModel spinnerNumberModel;
    private JPanel panel;

    public IntSpinner(String name, SpinnerNumberModel spinnerNumberModel, IntConsumer consumer) {
        this.label = new JLabel(name);
        this.spinnerNumberModel = spinnerNumberModel;
        this.spinner = new JSpinner();
        this.spinner.setModel(this.spinnerNumberModel);
        this.spinner.addChangeListener(l->consumer.accept(getValue()));
        ((JSpinner.DefaultEditor) this.spinner.getEditor()).getTextField().setColumns(7);
        this.panel = new JPanel();
        this.panel.add(label);
        this.panel.add(spinner);

    }

    public IntSpinner(String name, SpinnerNumberModel spinnerNumberModel, IntConsumer consumer, Supplier<Integer> supplier) {
        this(name, spinnerNumberModel,consumer);
        for (ChangeListener changeListener : this.spinner.getChangeListeners()) {
            this.spinner.removeChangeListener(changeListener);
        }
        this.spinner.addChangeListener(l->consumer.accept(supplier.get()));
    }

    @Override
    public JPanel getPanel() {
        return panel;
    }

    public int getValue() {
        return (Integer)this.spinner.getValue();
    }
}
