package utils;

import org.rspeer.runetek.api.commons.AWTUtil;

import java.awt.*;
import java.util.Arrays;
import java.util.Collection;

public class PaintUtils {
    public static void info(Graphics g, int x, int y, Collection<String> strings) {
        x += 10;
        for (String s : strings) {
            AWTUtil.drawBoldedString(g, s,x,y);
            y += g.getFont().getSize();
        }
    }

    public static void info(Graphics g, Collection<String> strings) {
        info(g, 0,0,strings);
    }

    public static void info(Graphics g, String... strings) {
        info(g, 0, 0, Arrays.asList(strings));
    }

    public static void info(Graphics g,int x, int y, String... strings) {
        info(g, x, y, Arrays.asList(strings));
    }

    public static void boundInfo(Graphics g, Rectangle r, String s) {
        int width = g.getFontMetrics().stringWidth(s);
        AWTUtil.drawBoldedString(g,s,(int)r.getCenterX()-width/2,(int)r.getCenterY());
    }
}
