package doubleclick.enchanter;

import org.rspeer.runetek.api.component.tab.Spell;

import static org.rspeer.runetek.api.component.tab.Spell.Modern.*;
import static utils.ID.*;

public enum EnchantSpell {
    LEVEL_1(LEVEL_1_ENCHANT,STAFF_OF_WATER),
    LEVEL_2(LEVEL_2_ENCHANT,STAFF_OF_AIR),
    LEVEL_3(LEVEL_3_ENCHANT,STAFF_OF_FIRE),
    LEVEL_4(LEVEL_4_ENCHANT,STAFF_OF_EARTH),
    LEVEL_5(LEVEL_5_ENCHANT,MUD_BATTLESTAFF),
    ;

    private final Spell spell;
    private final int staffId;

    EnchantSpell(Spell spell, int staffId) {
        this.spell = spell;
        this.staffId = staffId;
    }

    public Spell getSpell() {
        return spell;
    }

    public int getStaffId() {
        return staffId;
    }
}
