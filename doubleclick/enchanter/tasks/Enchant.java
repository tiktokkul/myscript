package doubleclick.enchanter.tasks;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.*;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import java.time.Duration;

import static doubleclick.enchanter.Enchanter.*;
import static utils.ID.COSMIC_RUNE;

public class Enchant extends Task {
    private int nextDelay = 7;
    @Override
    public boolean validate() {
        return Equipment.contains(CURR.get().getStaffId()) &&
                Inventory.containsAll(COSMIC_RUNE, CURR.get().getBaseId()) &&
                Skills.getLevel(Skill.MAGIC) >= CURR.get().getSpell().getLevelRequired() &&
                EXP_TRACKER.getLastExpTimer().exceeds(Duration.ofSeconds(nextDelay));
    }

    @Override
    public int execute() {
        nextDelay = Random.nextInt(6,10);
        Item item = Inventory.getFirst(CURR.get().getBaseId());
        if (Magic.cast(CURR.get().getSpell(),item)) {
            Log.info("Casting enchant " + nextDelay);
            Time.sleepUntil(()->EXP_TRACKER.getLastExpTimer().getElapsed().toMillis() < 500, 2000);
        }
        return getLoopReturn();
    }
}
