package doubleclick.enchanter.tasks;

import doubleclick.enchanter.EnchantItem;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Equipment;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import static doubleclick.enchanter.Enchanter.CURR;
import static doubleclick.enchanter.Enchanter.getLoopReturn;
import static utils.ID.COSMIC_RUNE;

public class Withdraw extends Task {
    @Override
    public boolean validate() {
        return playerHasStaff() ||
                !Inventory.containsAll(COSMIC_RUNE, CURR.get().getBaseId());
    }

    @Override
    public int execute() {
        if (Bank.isOpen()) {
            if (!bankHasRequiredItems()) {
                Log.info("Missing required items");
                return -1;
            }
            withdraw();
        } else {
            Bank.open();
            Time.sleepUntil(Bank::isOpen,2000);
        }
        return getLoopReturn();
    }

    /**
     * Checks if needs to perform action, then performs action.
     * Restarts the loop if attempted an action.
     */
    private void withdraw() {
        if (depositUnneededItems()) return;
        if (withdrawCosmicRunes()) return;
        if (withdrawStaff()) return;
        if (withdrawBaseJewellery()) return;
        if (Time.sleepUntil(() -> !validate(), 2000)) {
            Bank.close();
        }
    }

    /**
     * @return false if inventory only contains cosmics runes and maximum 1 of the correct staff
     */
    private boolean depositUnneededItems() {
        final int[] excluded;
        if (Inventory.getCount(CURR.get().getStaffId()) > 1) {
            excluded = new int[]{COSMIC_RUNE};
        } else {
            excluded = new int[]{COSMIC_RUNE, CURR.get().getStaffId()};
        }
        if (Inventory.isEmpty() || Inventory.containsOnly(excluded)) return false;
        Bank.depositAllExcept(excluded);
        return true;
    }

    /**
     * @return false if already have cosmic runes in inventory, else withdraw all cosmic runes
     */
    private boolean withdrawCosmicRunes() {
        if (Inventory.contains(COSMIC_RUNE)) return false;
        Bank.withdrawAll(COSMIC_RUNE);
        return true;
    }

    /**
     * @return false if already have staff in inventory or equipped, else withdraw a staff
     */
    private boolean withdrawStaff() {
        if (playerHasStaff()) return false;
         Bank.withdraw(CURR.get().getStaffId(),1);
        return true;
    }

    /**
     * @return false if already have jewellery in inventory, else withdraw jewellery
     */
    private boolean withdrawBaseJewellery() {
        if (Inventory.contains(CURR.get().getBaseId())) return false;
        Bank.withdrawAll(CURR.get().getBaseId());
        return true;
    }

    /**
     * @return true if bank has required items for current EnchantItem and has cosmic runes in bank or inventory
     */
    private boolean bankHasRequiredItems() {
        if (!hasAllItemReqs(CURR.get())) {
            if (!setNextEnchant()) return false;
        }
        return true;
    }

    private boolean hasAllItemReqs(EnchantItem e) {
        return e.getSpell().getLevelRequired() <= Skills.getCurrentLevel(Skill.MAGIC) &&
                !noneLeft(e.getBaseId()) &&
                !noneLeft(COSMIC_RUNE) &&
                (Equipment.contains(e.getStaffId()) || !noneLeft(e.getStaffId()));
    }

    private boolean setNextEnchant() {
        for (EnchantItem e : EnchantItem.values()) {
            if (hasAllItemReqs(e)) {
                Log.info("Next enchantable found " + e.name());
                CURR.set(e);
                return true;
            }
        }
        Log.info("No enchantable found");
        return false;
    }

    private boolean playerHasStaff() {
        return Equipment.contains(CURR.get().getStaffId()) || Inventory.contains(CURR.get().getStaffId());
    }

    private boolean noneLeft(int id) {
        return !Bank.contains(id) && !Inventory.contains(id);
    }
}
