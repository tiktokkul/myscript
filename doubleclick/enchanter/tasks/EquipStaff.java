package doubleclick.enchanter.tasks;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Equipment;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.script.task.Task;

import static doubleclick.enchanter.Enchanter.CURR;
import static doubleclick.enchanter.Enchanter.getLoopReturn;

public class EquipStaff extends Task {
    @Override
    public boolean validate() {
        return !Equipment.contains(CURR.get().getStaffId()) &&
                Inventory.containsAll(CURR.get().getStaffId());
    }

    @Override
    public int execute() {
        Item staff = Inventory.getFirst(CURR.get().getStaffId());
        if (staff != null && staff.interact("Wield")) {
            Time.sleepUntil(() -> Equipment.contains(CURR.get().getStaffId()), 2500);
        }
        return getLoopReturn();
    }
}