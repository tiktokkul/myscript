package doubleclick.enchanter;

import api.price.PriceCheck;
import org.rspeer.runetek.api.component.tab.Spell;

import static doubleclick.enchanter.EnchantSpell.*;
import static utils.ID.*;

public enum EnchantItem {
    RECOIL(SAPPHIRE_RING,RING_OF_RECOIL,LEVEL_1),
    GAMES(SAPPHIRE_NECKLACE,GAMES_NECKLACE8,LEVEL_1),
    CLAY(SAPPHIRE_BRACELET,BRACELET_OF_CLAY,LEVEL_1),
    MAGIC(SAPPHIRE_AMULET,AMULET_OF_MAGIC,LEVEL_1),

    PURSUIT(OPAL_RING,RING_OF_PURSUIT,LEVEL_1),
    EXPED(OPAL_BRACELET,EXPEDITIOUS_BRACELET,LEVEL_1),
    DODGY(OPAL_NECKLACE,DODGY_NECKLACE,LEVEL_1),
    BOUNTY(OPAL_AMULET,AMULET_OF_BOUNTY,LEVEL_1),

    DUELING(EMERALD_RING,RING_OF_DUELING8, LEVEL_2),
    BINDING(EMERALD_NECKLACE,BINDING_NECKLACE,LEVEL_2),
    CWAR(EMERALD_BRACELET,CASTLE_WARS_BRACELET3, LEVEL_2),
    DEFENCE(EMERALD_AMULET,AMULET_OF_DEFENCE,LEVEL_2),
    PASSAGE(JADE_NECKLACE,NECKLACE_OF_PASSAGE5,LEVEL_2),
    CHEMISTRY(JADE_AMULET,AMULET_OF_CHEMISTRY,LEVEL_2),
    RETURNING(JADE_RING,RING_OF_RETURNING5,LEVEL_2),
    FLAMTAER(JADE_BRACELET,FLAMTAER_BRACELET,LEVEL_2),


    FORGING(RUBY_RING,RING_OF_FORGING,LEVEL_3),
    INOCULATION(RUBY_BRACELET,INOCULATION_BRACELET,LEVEL_3),
    STRENGTH(RUBY_AMULET,AMULET_OF_STRENGTH,LEVEL_3),
    EFARITAY(TOPAZ_RING,EFARITAYS_AID,LEVEL_3),
    SLAUGHTER(TOPAZ_BRACELET,BRACELET_OF_SLAUGHTER,LEVEL_3),
    FAITH(TOPAZ_NECKLACE,NECKLACE_OF_FAITH,LEVEL_3),
    BURNING(TOPAZ_AMULET,BURNING_AMULET5,LEVEL_3),

    POWER(DIAMOND_AMULET,AMULET_OF_POWER,LEVEL_4),
    LIFE(DIAMOND_RING,RING_OF_LIFE,LEVEL_4),
    PHOENIX(DIAMOND_NECKLACE,PHOENIX_NECKLACE,LEVEL_4),
    ABYSSAL(DIAMOND_BRACELET,ABYSSAL_BRACELET5,LEVEL_4),

    GLORY(DRAGONSTONE_AMULET,AMULET_OF_GLORY,LEVEL_5),
    SKILLS(DRAGON_NECKLACE,SKILLS_NECKLACE,LEVEL_5),
    COMBAT(DRAGONSTONE_BRACELET,COMBAT_BRACELET,LEVEL_5),
    WEALTH(DRAGONSTONE_RING,RING_OF_WEALTH,LEVEL_5),

    ;
    private final int baseId;
    private final int productId;
    private final EnchantSpell spell;

    EnchantItem(int baseId, int productId, EnchantSpell spell) {
        this.baseId = baseId;
        this.productId = productId;
        this.spell = spell;
    }

    public int getBaseId() {
        return baseId;
    }

    public int getProductId() {
        return productId;
    }

    public Spell getSpell() {
        return spell.getSpell();
    }

    public int getStaffId() {
        return spell.getStaffId();
    }

    public int profit() {
        int product = PriceCheck.getLowestGePrice(productId);
        int cosmicRune = Math.max(115, PriceCheck.getLowestGePrice(COSMIC_RUNE));
        int price = PriceCheck.getLowestGePrice(baseId);
        if (product == 0 || cosmicRune == 0 || price == 0) return 0;
        return product - (price + cosmicRune);
    }
}
