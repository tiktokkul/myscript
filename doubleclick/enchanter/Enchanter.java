package doubleclick.enchanter;

import api.ExpTracker;
import doubleclick.enchanter.tasks.Enchant;
import doubleclick.enchanter.tasks.EquipStaff;
import doubleclick.enchanter.tasks.Withdraw;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.listeners.SkillListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.runetek.event.types.SkillEvent;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;
import utils.PaintUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
@ScriptMeta( name = "Progressive Enchanter", developer = "DoubleClick", desc = "Enchants items and equips staves")
public class Enchanter extends TaskScript implements RenderListener, SkillListener {
    public static final ExpTracker EXP_TRACKER = new ExpTracker(Skill.MAGIC);
    public static final AtomicReference<EnchantItem> CURR = new AtomicReference<>(EnchantItem.RECOIL);
    public static int getLoopReturn() {
        return Random.high(500, 2000);
    }
    private int totalProfit = 0;

    @Override
    public void onStart() {
        Game.getEventDispatcher().register(EXP_TRACKER);
        submit(new EquipStaff(), new Enchant(), new Withdraw());
        for (EnchantItem enchantItem : EnchantItem.values()) {
            Log.info(enchantItem.name() + " Profit: " + enchantItem.profit());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Game.getEventDispatcher().deregister(EXP_TRACKER);
    }

    private final List<String> strings = new ArrayList<>();
    @Override
    public void notify(RenderEvent r) {
        Graphics g = r.getSource();
        strings.clear();
        strings.add("Level " + Skills.getLevel(EXP_TRACKER.getSkill()));
        strings.add("Runtime "+ EXP_TRACKER.getTimer().toElapsedString());
        strings.add("Last exp " + EXP_TRACKER.getLastExpTimer().toElapsedString());
        strings.add("Profit " +totalProfit + "/" + (int) EXP_TRACKER.getTimer().getHourlyRate(totalProfit));
        strings.add(EXP_TRACKER.stats());
        PaintUtils.info(g,0,35,strings);
    }

    @Override
    public void notify(SkillEvent s) {
        if (s.getSource() == Skill.MAGIC)
        totalProfit += Math.max(CURR.get().profit(),0);
    }
}
