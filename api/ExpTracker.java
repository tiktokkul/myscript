package api;

import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.event.listeners.SkillListener;
import org.rspeer.runetek.event.types.SkillEvent;

public class ExpTracker implements SkillListener {
    private StopWatch timer;
    private StopWatch lastExpTimer;
    private int experience = 0;
    private final Skill skill;

    public ExpTracker(Skill skill) {
        this.skill = skill;
        this.timer = StopWatch.start();
        this.lastExpTimer = StopWatch.start();
    }

    @Override
    public void notify(SkillEvent s) {
        if (!Game.isLoggedIn()) return;
        if (Game.getState() == Game.STATE_LOGGING_IN) return;
        if (s.getType() == SkillEvent.TYPE_EXPERIENCE && s.getSource() == skill) {
            experience += Math.abs(s.getCurrent() - s.getPrevious());
            lastExpTimer.reset();
        }
    }

    public int getExperience() {
        return experience;
    }

    public Skill getSkill() {
        return skill;
    }

    public StopWatch getTimer() {
        return timer;
    }

    public StopWatch getLastExpTimer() {
        return lastExpTimer;
    }

    public String stats() {
        return skill.name() + " " + experience + " / " + (int)timer.getHourlyRate(experience);
    }
}
