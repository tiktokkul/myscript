package api.price;


import com.allatori.annotations.DoNotRename;
import com.google.gson.annotations.SerializedName;

@DoNotRename
public class AlchValue {

    @DoNotRename
    private String name;
    @DoNotRename
    @SerializedName("store")
    private int alchValue;

    public String getName() {
        return name;
    }

    public int getAlchValue() {
        return (int)(alchValue*0.6);
    }
}
