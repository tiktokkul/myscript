package api.price;

import com.acuitybotting.common.utils.ExecutorUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.rspeer.ui.Log;
import utils.ID;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class PriceCheck {

    private static Gson g = new Gson();
    private static Map<String, Integer> nameItemMapping = new HashMap<>();
    private static Map<Integer, ItemPrice> prices = new HashMap<>();
    private static Map<Integer, AlchValue> alchValues = new HashMap<>();
    private static Map<Integer, Integer> buyLimits = new HashMap<>();
    private static int reloadMinutes = 30;
    private static boolean isReloadEnabled = true;

    private static ScheduledThreadPoolExecutor executor = ExecutorUtil.newScheduledExecutorPool(1, Throwable::printStackTrace);
    private static ScheduledFuture<?> task;

    public static Map<Integer, ItemPrice> getPrices() {
        if (prices.size() == 0) {
            reload();
        }
        return prices;
    }

    public static Map<Integer, AlchValue> getAlchValues() {
        if (alchValues.size() == 0) {
            reload();
        }
        return alchValues;
    }

    public static ItemPrice getItemPrice(String name) {
        if(prices.size() == 0) {
            reload();
        }
        int id = nameItemMapping.getOrDefault(name.toLowerCase(), -1);
        return id == -1 ? null : getItemPrice(id);
    }

    public static ItemPrice getItemPrice(int id) {
        if(prices.size() == 0) {
            reload();
        }
        return prices.getOrDefault(id, null);
    }

    public static int getLowestGePrice(int id) {
        if(prices.size() == 0) {
            reload();
        }
        ItemPrice p = getItemPrice(id);
        if (p == null) {
            return 0;
        }
        return p.lowestPrice();
    }

    public static int getLowestGePrice(String name) {
        if(prices.size() == 0) {
            reload();
        }
        ItemPrice p = getItemPrice(name);
        if (p == null) {
            return 0;
        }
        return p.lowestPrice();
    }

    public static int getAlchValue(int id) {
        if(alchValues.size() == 0) {
            reload();
        }
        if (alchValues.containsKey(id)) {
            return alchValues.getOrDefault(id, null).getAlchValue();
        }
        return 0;
    }

    public static Integer getBuyLimit(int id) {
        if(buyLimits.size() == 0) {
            buyLimits = loadGELimits();
        }
        return buyLimits.getOrDefault(id, 20000);
    }

    public static String getName(int id) {
        if (prices.size() == 0) {
            reload();
        }
        ItemPrice p = getItemPrice(id);
        if (p == null) return "";
        return p.getName();
    }

    public static void reload() {
        if(!isReloadEnabled && (prices.size() > 0 && alchValues.size() > 0)) {
            return;
        }
        if(task == null && isReloadEnabled) {
            task = executor.scheduleAtFixedRate(PriceCheck::reload, reloadMinutes, reloadMinutes, TimeUnit.MINUTES);
        }
        try {
            HttpResponse<String> node = Unirest.get("https://rsbuddy.com/exchange/summary.json").asString();
            if(node.getStatus() != 200) {
//                System.out.println(node.getBody());
                Log.severe("PriceCheck", "Failed to load prices. Result: " + node.getBody());
                return;
            }
            JsonObject o = g.fromJson(node.getBody(), JsonObject.class);
            for (String s : o.keySet()) {
                ItemPrice price = g.fromJson(o.get(s).getAsJsonObject(), ItemPrice.class);
                int id = Integer.parseInt(s);
                String name = price.getName().toLowerCase();
                nameItemMapping.remove(name);
                nameItemMapping.put(name, id);
                prices.remove(id);
                prices.put(id, price);
            }
            node = Unirest.get("https://rsbuddy.com/exchange/names.json").asString();
            if(node.getStatus() != 200) {
//                System.out.println(node.getBody());
                Log.severe("PriceCheck", "Failed to load alch values. Result: " + node.getBody());
                return;
            }
            o = g.fromJson(node.getBody(), JsonObject.class);
            for (String s : o.keySet()) {
                AlchValue price = g.fromJson(o.get(s).getAsJsonObject(), AlchValue.class);
                int id = Integer.parseInt(s);
                alchValues.remove(id);
                alchValues.put(id, price);
            }
        } catch (UnirestException e) {
            e.printStackTrace();
            Log.severe(e);
        }
    }

    public static void dispose() {
        task.cancel(true);
        executor.shutdown();
    }

    public static void setShouldReload(boolean value) {
        isReloadEnabled = value;
    }


    private static final TypeToken<Map<Integer, Integer>> BUY_LIMIT_TOKEN = new TypeToken<Map<Integer, Integer>>()
    {
    };

    private static Map<Integer, Integer> loadGELimits()
    {
        final InputStream geLimitData = PriceCheck.class.getResourceAsStream("ge_limits.json");
        final Map<Integer, Integer> itemGELimits = g.fromJson(new InputStreamReader(geLimitData), BUY_LIMIT_TOKEN.getType());
        Log.info("Loaded {} limits", itemGELimits.size());
        return itemGELimits;
    }

    public static void main(String[] args) {
    }
}