package api.price;

import com.allatori.annotations.DoNotRename;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@DoNotRename
public class ItemPrice {

    @DoNotRename
    private String name;
    @DoNotRename
    private boolean members;
    @DoNotRename
    @SerializedName("buy_average")
    private int buyAverage;
    @DoNotRename
    @SerializedName("sell_average")
    private int sellAverage;
    @DoNotRename
    @SerializedName("overall_average")
    private int overallAverage;
    @DoNotRename
    @SerializedName("buy_quantity")
    private int buyQuantity;
    @DoNotRename
    @SerializedName("sell_quantity")
    private int sellQuantity;
    @DoNotRename
    @SerializedName("overall_quantity")
    private int overallQuantity;

    public String getName() {
        return name;
    }

    public boolean isMembers() {
        return members;
    }

    public int getBuyAverage() {
        return buyAverage;
    }

    public int getSellAverage() {
        return sellAverage;
    }

    public int getOverallAverage() {
        return overallAverage;
    }

    public int getBuyQuantity() {
        return buyQuantity;
    }

    public int getSellQuantity() {
        return sellQuantity;
    }

    public int getOverallQuantity() {
        return overallQuantity;
    }

    public int lowestPrice() {
        List<Integer> list = new ArrayList<>();
        if (getOverallAverage() != 0) list.add(getOverallAverage());
        if (getSellAverage() != 0) list.add(getSellAverage());
        if (getBuyAverage() != 0) list.add(getBuyAverage());
        return list.stream().min(Comparator.naturalOrder()).orElse(0);
    }


    public int lowestQuantity() {
        List<Integer> list = new ArrayList<>();
        if (getOverallQuantity() != 0) list.add(getOverallQuantity());
        if (getSellQuantity() != 0) list.add(getSellQuantity());
        if (getBuyQuantity() != 0) list.add(getBuyQuantity());
        return list.stream().min(Comparator.naturalOrder()).orElse(0);
    }
}
