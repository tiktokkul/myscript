package api.wrappers;

import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

import java.util.Comparator;

public class AreaWrapper {
    public static Position nearestTileInArea(Area area) {
        return area.getTiles().stream().sorted(Comparator.comparingDouble(Position::distance)).findFirst().orElse(null);
    }
}
