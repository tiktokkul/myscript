package api.wrappers;

import org.rspeer.runetek.adapter.component.InterfaceComponent;
import org.rspeer.runetek.api.Worlds;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.WorldHopper;

import java.util.function.Predicate;

public class WorldHopperWrapper {
    private static final int WORLD_SWITCH_WARNING = 193;
    private static final Predicate<InterfaceComponent> PVP_WORLD_SWITCH_PREDICATE = ic -> ic.getText().equals("Switch to it");

    public static void hopTo(int world) {
        InterfaceComponent component = Interfaces.getFirst(WORLD_SWITCH_WARNING,PVP_WORLD_SWITCH_PREDICATE,true);
        if (component != null) {
            component.click();
        } else {
            WorldHopper.hopTo(world);
        }
        Time.sleepUntil(()-> Worlds.getCurrent() == world, 2000);
    }
}
