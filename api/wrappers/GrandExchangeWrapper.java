package api.wrappers;

import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.GrandExchange;
import org.rspeer.runetek.api.component.GrandExchangeSetup;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.providers.RSGrandExchangeOffer;
import org.rspeer.ui.Log;

public class GrandExchangeWrapper {
    public static final Position GE_POSITION = new Position(3165, 3486, 0);
    /**
     * @pre Grand exchange openGE, at least 1 openGE slot, valid id, price & quantity > 0
     * Return true if validated item, price and quantity and confirms
     */
    public static boolean buyItem(int id, int price, int quantity) {
        switch (GrandExchange.getView()) {
            case CLOSED:
            case SELL_OFFER:
            case OVERVIEW:
                if (GrandExchange.getFirstEmpty() != null)
                    GrandExchange.getFirstEmpty().create(RSGrandExchangeOffer.Type.BUY);
//                Log.info("Setting buy offer");
                if (!Time.sleepUntil(() -> GrandExchange.getView() == GrandExchange.View.BUY_OFFER, 1000)) return false;
        }
        if (GrandExchangeSetup.getItem() == null || GrandExchangeSetup.getItem().getId() != id) {
//            Log.info("Setting item");
            GrandExchangeSetup.setItem(id);
            Time.sleepUntil(() -> GrandExchangeSetup.getItem() != null &&
                    GrandExchangeSetup.getItem().getId() == id, 1000);
        }
        if (GrandExchangeSetup.getPricePerItem() != price) {
//            Log.info("Setting price " + price);
            GrandExchangeSetup.setPrice(price);
            Time.sleepUntil(() -> GrandExchangeSetup.getPricePerItem() == price, 1000);
        }

        if (GrandExchangeSetup.getQuantity() != quantity) {
//            Log.info("Setting quantity " + quantity);
            GrandExchangeSetup.setQuantity(quantity);
            Time.sleepUntil(()->GrandExchangeSetup.getQuantity() == quantity,1000);
        }
        if (GrandExchangeSetup.getItem() == null ||
                GrandExchangeSetup.getItem().getId() != id ||
                GrandExchangeSetup.getPricePerItem() != price ||
                GrandExchangeSetup.getQuantity() != quantity) {
            Log.info("Failed to set GESetup settings");
            return false;
        }
        boolean success = GrandExchangeSetup.confirm();
        Time.sleep(500,2000);
        return success;
    }

    public static boolean openGE() {
        Npc geMan = Npcs.getNearest(Npcs.newQuery().actions("Exchange").names("Grand Exchange Clerk"));
        if (geMan != null) {
            Log.info("Opening GE");
            geMan.interact("Exchange");
            return Time.sleepUntil(GrandExchange::isOpen,2500);
        } else {
            return MovementWrapper.walkToWait(GE_POSITION);
        }
    }
}
