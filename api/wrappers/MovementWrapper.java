package api.wrappers;

import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;

import java.util.function.BooleanSupplier;

public class MovementWrapper {
    public static boolean walkToWait(Position p) {
        return walkToWait(p,()->false);
    }

    public static boolean walkToWait(Position p, BooleanSupplier exit) {
        if (p.distance() == 0) return true;
        Movement.walkTo(p);
        if (!Time.sleepUntil(Movement::isDestinationSet, 1000)) return false;
        int distanceToDest = p.distance() < 10 ? 3 : 5;
        Time.sleepUntil(()->!Movement.isDestinationSet() ||
                Movement.getDestination().distance() < distanceToDest ||
                exit.getAsBoolean(), 5000);
        return true;
    }

    private static StopWatch toggleTimer = StopWatch.start();
    public static boolean toggleRun(boolean on) {
        if (toggleTimer.getElapsed().getSeconds() < 3) return true;
        if (Movement.isRunEnabled() == on) return true;
        if (on && Movement.getRunEnergy() < 30) return false;

        toggleTimer.reset();
        Movement.toggleRun(on);
        return Time.sleepUntil(()->Movement.isRunEnabled() == on,500);
    }
}
